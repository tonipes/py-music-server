function AlertData(type, message){
	this.type = type;
	this.message = message;
}

function showalert(message, type, placeid, hidetime) {
	hidetime = hidetime || 5000; // default time
	//$('#alertdiv').remove();
  $.get('/template/alert.html', function(template, textStatus, jqXhr) {
		var html = Mustache.to_html(template, new AlertData(type, message));
		$('#' + placeid).replaceWith(html)
	});
	//var html = Mustache.to_html(alertTemplate, new AlertData(type, message));
	//$('#' + placeid).append(html)
	//setTimeout(function() {
	//  $('#alertdiv').remove();
	//}, hidetime);
	
}