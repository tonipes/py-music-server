function loginValidate(){
  if (document.getElementById("usernameField").value=='' || document.getElementById("passwordField").value==''){
    showalert('Check username and password', 'alert-danger', 'alert_placeholder');
    return false;
  }
  $( '#loginform' ).submit();
  return true;
}

$(document).ready(function(){
  $('#loginBtn').click(function(){
    loginValidate();
  });

  $('#passwordField').keyup(function(event){
    if(event.keyCode == 13){
        $("#loginBtn").click();
    }
	});
});

