function api(action, data, successfunc, errorfunc){
  $.ajax({
    url: 'api/handle/'+action,
    context: $(this),
    data: {'data': JSON.stringify(data)},
    type: 'POST',
    success: successfunc,
    error: errorfunc
  });
}

function rebuildDatabase(){
  var success = function(){
    loadPlaylists();
  }
  var error = function(){

  }
  api('rebuilddatabase', {'path':'/'}, success, error)
}

function loadPlaylists(){
	var success = function(data){
    console.log("Playlists loaded successfully!");

    $.get('/template/playlist_container.html', function(template, textStatus, jqXhr) {
      console.log("getting playlist container");
      var d = $.parseJSON(data)['data']
      console.log(d);
      var html = Mustache.to_html(template, d);
      $('#playlist_panel').html(html);
    });

  }
  var error = function(){console.log("Could not load playlists!")}

  api('getplaylists',{},success,error);
}

function loadFolderMedia(path){
  path = path || '/';
  console.log("loading folder media for " + path);
  var success = function(data){
    console.log("Folder media loaded successfully!");

    $.get('/template/media_container_folder.html', function(template, textStatus, jqXhr) {
      var d = $.parseJSON(data)['data']
      console.log(d);
      var html = Mustache.to_html(template, d);
      $('#media_panel').html(html);
    });
  }
  var error = function(){console.log("Could not load playlist media!")}
  api('getfoldermedia'+ '/' + path,{}, success, error);
}

function loadPlaylistMedia(playlistid){
  console.log("loading playlistmedia");

  var success = function(data){
    console.log("Playlist media loaded successfully!");

    $.get('/template/media_container_playlist.html', function(template, textStatus, jqXhr) {
      var d = $.parseJSON(data)['data']
      console.log(d);
      var html = Mustache.to_html(template, d);
      $('#media_panel').html(html);
    });
  }

  var error = function(){console.log("Could not load playlist media!")}
  api('getplaylistmedia',{'id':playlistid}, success, error);
}

$(document).ready(function(){
  loadPlaylists();
  loadFolderMedia();
});
