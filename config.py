# -*- encoding: UTF-8 -*-
import os

MUSIC_PATH = '/mnt/usbstorage/music'

APP_PATH = os.path.dirname(__file__) + '/'

RES_PATH = APP_PATH + 'res'
STATIC_PATH = RES_PATH + '/static'
TEMPLATE_PATH = STATIC_PATH + '/template'

DATA_PATH = '/home/cherrymusic/.local/share/pyserver'
DB_PATH = DATA_PATH + '/db'
