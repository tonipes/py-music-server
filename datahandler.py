# -*- encoding: UTF-8 -*-
import cherrypy
from config import MUSIC_PATH
from database import cache
from cherrypy import tools
from auth import AuthController, require, member_of, name_is
import database
import json

class DataHandler(object):
	def __init__(self):
		self.actions = {
		'getplaylists': self.getPlaylists,
		'getplaylistmedia': self.getPlaylistMedia,
		'getfoldermedia': self.getFolderMedia,
		'rebuilddatabase': self.updateDatabase
		}

	@cherrypy.expose
	@require()
	def index(self):
		return "What? Try 'api/handle'"

	@cherrypy.expose
	@require()
	@tools.json_out()
	def handle(self, *args, **kwargs):
		print(args)
		action = args[0] if args else ''
		if not action in self.actions:
			return "Error: no such action. '%s'" % action
		handler = self.actions[action]
		handler_args = {}
		if 'data' in kwargs:
			handler_args = json.loads(kwargs['data'])
		return json.dumps({'data': handler(*args[1:], **handler_args)})

	@require()
	def getPlaylists(self, *args, **kwargs):
		cursor = cherrypy.thread_data.cachedb.cursor()
		cursor.execute("SELECT title, id FROM playlists")
		
		playlists = []

		for row in cursor:
			playlists.append({'title':row[0], 'id' : row[1]})
		return {'playlist?': playlists}

	@require(member_of("admingroup"))
	def updateDatabase(self, *args, **kwargs):
		# TODO: use path argument!
		cache.rebuildFullCache()

	@require()
	def getFolderMedia(self, *args, **kwargs):
		path = '/'.join(list(args))
		print('path: ' + path)

		cursor = cherrypy.thread_data.cachedb.cursor()

		cursor.execute("""SELECT id, parentpath, foldername FROM folders WHERE parentpath=?""", (unicode(path,'UTF-8'),))

		folder = []
		for row in cursor:
			folder.append({'id':row[0], 'parent':row[1], 'name':row[2]})

		parentpath = cache.getParentPath(path)
		foldername = cache.getFolderName(path)
		cursor.execute("""SELECT id FROM folders WHERE parentpath=? AND foldername=?""", (unicode(parentpath,'UTF-8'), unicode(foldername,'UTF-8'),))
		fetch = cursor.fetchone()

		media = []

		if fetch:
			folderid = fetch[0]
			cursor.execute("""SELECT _TIT2, _TALB, _TPE1, id FROM songs WHERE parent =?""", (folderid,))
			
			for row in cursor:
				media.append({'TIT2':row[0], 'TALB' : row[1], 'TPE1' : row[2], 'id' : row[3]})

		return {'media?': media, 'folder?': folder}

	def getPlaylistMedia(self, *args, **kwargs):
		if not 'id' in kwargs:
			print("No id found")
		id = kwargs['id']
		cursor = cherrypy.thread_data.cachedb.cursor()
		cursor.execute("""SELECT songs._TIT2, songs._TALB, songs._TPE1, songs.id FROM songs,tracks WHERE songs.id = tracks.songid AND tracks.playlistid =?""", 
			(id,))
		media = []
		for row in cursor:
			media.append({'TIT2':row[0], 'TALB' : row[1], 'TPE1' : row[2], 'id' : row[3]})
		return {'media?': media}

