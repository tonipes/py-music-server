import os
from config import RES_PATH

def loadTemplate(path):
	return loadFile(os.path.join('static/template', path))

def loadFile(path):
	return unicode(open(os.path.join(RES_PATH, path)).read())
