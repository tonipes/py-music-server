# -*- encoding: UTF-8 -*-

import cherrypy
import os
import sqlite3  
import threading
import database
from loader import loadTemplate
from datahandler import DataHandler
from database import cache
from config import APP_PATH
from config import RES_PATH
from auth import AuthController, require, member_of, name_is

class Root(object):
	auth = AuthController()
	api = DataHandler()

	@cherrypy.expose
	@require()
	def index(self):
		template = loadTemplate('index.html')
		return template

if __name__ == '__main__':
	database.initdb(False)
	cherrypy.engine.subscribe('start_thread', database.connect)    
	cherrypy.config.update(os.path.join(APP_PATH + 'server.conf'))
	cherrypy.quickstart(Root(), '/')
	cherrypy.engine.start()
	#cache.rebuildFullCache()