INSERT INTO playlists(title) VALUES('sample 1');
INSERT INTO tracks(playlistid, songid) VALUES(1,900);
INSERT INTO tracks(playlistid, songid) VALUES(1,902);
INSERT INTO tracks(playlistid, songid) VALUES(1,903);
INSERT INTO tracks(playlistid, songid) VALUES(1,904);

INSERT INTO playlists(title) VALUES('sample 1');
INSERT INTO tracks(playlistid, songid) VALUES(2,901);
INSERT INTO tracks(playlistid, songid) VALUES(2,902);
INSERT INTO tracks(playlistid, songid) VALUES(2,903);
INSERT INTO tracks(playlistid, songid) VALUES(2,904);

INSERT INTO playlists(title) VALUES('sample 2');
INSERT INTO tracks(playlistid, songid) VALUES(3,905);
INSERT INTO tracks(playlistid, songid) VALUES(3,906);
INSERT INTO tracks(playlistid, songid) VALUES(3,907);
INSERT INTO tracks(playlistid, songid) VALUES(3,908);

INSERT INTO playlists(title) VALUES('singlesong');
INSERT INTO tracks(playlistid, songid) VALUES(4,910);
INSERT INTO tracks(playlistid, songid) VALUES(4,910);
INSERT INTO tracks(playlistid, songid) VALUES(4,910);
INSERT INTO tracks(playlistid, songid) VALUES(4,910);

INSERT INTO playlists(title) VALUES('sample 4');
INSERT INTO tracks(playlistid, songid) VALUES(5,921);
INSERT INTO tracks(playlistid, songid) VALUES(5,922);
INSERT INTO tracks(playlistid, songid) VALUES(5,923);
INSERT INTO tracks(playlistid, songid) VALUES(5,924);

INSERT INTO playlists(title) VALUES('sample 5');
INSERT INTO tracks(playlistid, songid) VALUES(6,931);
INSERT INTO tracks(playlistid, songid) VALUES(6,932);
INSERT INTO tracks(playlistid, songid) VALUES(6,933);
INSERT INTO tracks(playlistid, songid) VALUES(6,934);

INSERT INTO playlists(title) VALUES('sample 6');
INSERT INTO tracks(playlistid, songid) VALUES(7,941);
INSERT INTO tracks(playlistid, songid) VALUES(7,942);
INSERT INTO tracks(playlistid, songid) VALUES(7,943);
INSERT INTO tracks(playlistid, songid) VALUES(7,944);