CREATE TABLE IF NOT EXISTS users(
	username TEXT NOT NULL PRIMARY KEY ,
	usergroup TEXT NOT NULL DEFAULT 'user',
	password TEXT NOT NULL
);

INSERT OR IGNORE INTO users(username, usergroup, password) VALUES('admin', 'admingroup', 'qwerty');