# -*- encoding: UTF-8 -*-
import os
import os.path
import sqlite3
import time
import cherrypy
from os.path import relpath
from mutagen.id3 import ID3
from config import MUSIC_PATH
import database 

audio_exts = ['mp3']
image_exts = ['jpg', 'jpeg', 'png']
video_exts = []
folder_exclude = ['Covers', 'Scans']

def rebuildFullCache():
	startTime = time.time()

	print('Rebuilding song cache...')
	cursor = cherrypy.thread_data.cachedb.cursor()

	cursor.executescript(database.getSqlScript('sql/cache/drop.sql'))
	cursor.executescript(database.getSqlScript('sql/cache/create.sql'))
	cursor.executescript(database.getSqlScript('sql/cache/debug_fill.sql'))

	#insert root
	cursor.execute("INSERT INTO folders(parentpath,foldername) VALUES(?,?)", ('','',))

	for root, dirs, files in os.walk(MUSIC_PATH, topdown=True, followlinks=True):
		for name in dirs:
			if not name.startswith('.'):
				relpath = os.path.relpath(root, MUSIC_PATH)
				path = '' if relpath is '.' else relpath
				addDir(path, name)
		for name in files:
			if not name.startswith('.'):
				relpath = os.path.relpath(root, MUSIC_PATH)
				path = '' if relpath is '.' else relpath
				addFile(path, name)

	cherrypy.thread_data.cachedb.commit()

	endTime = time.time()
	print('Cache rebuilding took ' + str(endTime - startTime) + ' seconds.')

def addFile(parent, name):
	cursor = cherrypy.thread_data.cachedb.cursor()
	p_parentpath = getParentPath(parent)
	p_foldername = getFolderName(parent)
	cursor.execute("SELECT id FROM folders WHERE parentpath=? AND foldername=?",(unicode(p_parentpath, 'utf-8'), unicode(p_foldername, 'utf-8')))
	_filetype = unicode(getExtension(name))
	if _filetype in audio_exts:
		_parent = cursor.fetchone()[0]
		_filename = unicode(name,'utf-8')

		sData = ID3(getAbsPath(parent,name))

		_TIT2 = unicode(str(sData['TIT2']),'utf-8') if 'TIT2' in sData else ''
		_TALB = unicode(str(sData['TALB']),'utf-8') if 'TALB' in sData else ''
		_TCOM = unicode(str(sData['TCOM']),'utf-8') if 'TCOM' in sData else ''
		_TYER = unicode(str(sData['TYER']),'utf-8') if 'TYER' in sData else ''
		_TEXT = unicode(str(sData['TEXT']),'utf-8') if 'TEXT' in sData else ''
		_TPE1 = unicode(str(sData['TPE1']),'utf-8') if 'TPE1' in sData else ''
		_TPE2 = unicode(str(sData['TPE2']),'utf-8') if 'TPE2' in sData else ''
		_TPE3 = unicode(str(sData['TPE3']),'utf-8') if 'TPE3' in sData else ''
		_TPOS = unicode(str(sData['TPOS']),'utf-8') if 'TPOS' in sData else ''
		_TRCK = unicode(str(sData['TRCK']),'utf-8') if 'TRCK' in sData else ''
		cursor.execute("""INSERT INTO songs(parent, filename, filetype, _TIT2, _TALB, _TCOM, _TYER, _TEXT, _TPE1, _TPE2, _TPE3, _TPOS, _TRCK) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)""", 
			(_parent, _filename,_filetype,_TIT2, _TALB, _TCOM, _TYER, _TEXT, _TPE1, _TPE2, _TPE3, _TPOS, _TRCK,))

def addDir(parent, name):
	cursor = cherrypy.thread_data.cachedb.cursor()
	cursor.execute("INSERT INTO folders(parentpath,foldername) VALUES(?,?)", (unicode(parent, 'utf-8'), unicode(name, 'utf-8')))

# helpers
def getFolderName(path):
	return os.path.basename(os.path.normpath(path))

def getParentPath(path):
	return os.path.dirname(path)

def getExtension(name):
	return os.path.splitext(name)[1][1:].strip().lower()

def getAbsPath(root, name):
	return os.path.join(MUSIC_PATH, os.path.join(root,name))




