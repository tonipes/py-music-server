# -*- encoding: UTF-8 -*-
import os
import config
import sqlite3
import threading
import cherrypy

userdbPath = os.path.join(config.DB_PATH,'user.db')
cachedbPath = os.path.join(config.DB_PATH,'cache.db')

def connect(thread_index):
	cherrypy.thread_data.userdb = sqlite3.connect(userdbPath)
	cherrypy.thread_data.cachedb = sqlite3.connect(cachedbPath)

def initdb(debug=False):
	# User init
	sqlU = getSqlScript('sql/user/create.sql')
	connU = sqlite3.connect(userdbPath,check_same_thread=False)
	cU = connU.cursor()
	cU.executescript(sqlU)

	if debug:
		sql = getSqlScript('sql/user/debug_fill.sql')
		cU.executescript(sql)
	connU.commit()
	connU.close()

	# Cache init
	sqlC = getSqlScript('sql/cache/create.sql')
	connC = sqlite3.connect(cachedbPath,check_same_thread=False)
	cC = connC.cursor()
	cC.executescript(sqlC)
	
	if debug:
		sql = getSqlScript('sql/cache/debug_fill.sql')
		cC.executescript(sql)
	connC.commit()
	connC.close()

def getSqlScript(path):
	return unicode(open(os.path.join(os.path.dirname(__file__), path)).read())